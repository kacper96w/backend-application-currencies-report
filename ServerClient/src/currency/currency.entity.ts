import {
  IsDate,
  IsNumber,
  IsOptional,
  MinLength,
  Max,
  IsString,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCurrencyDto {
  @ApiProperty()
  @MinLength(1)
  name: string;

  @ApiProperty()
  @IsString()
  currency_value: string;

  @ApiProperty()
  @IsString()
  added_time: string;
}

@Entity()
export class CurrencyEntity {
  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'currency_id',
  })
  id: number;

  @ApiProperty({ example: 'USD' })
  @Column({
    nullable: false,
    default: '',
  })
  name: string;

  @ApiProperty({ example: 10 })
  @Column({
    nullable: true,
  })
  currency_value: string;

  @ApiProperty({ example: '2022-12-28' })
  @Column({
    nullable: true,
  })
  added_time: string;
}

export class ErrorResponse {
  statusCode: number;
  message: string;
  error: string;
}

export class GetCurrenciesDto {
  @IsNumber()
  @IsOptional()
  page?: number = 1;

  @IsNumber()
  @Max(5)
  @IsOptional()
  @Transform((data) => parseInt(data.value))
  @Type()
  pageSize?: number = 2;

  sortBy?: string = 'name';
  sortDir?: SortDir = SortDir.ASC;
}

export enum SortDir {
  ASC = 'asc',
  DESC = 'desc',
}
