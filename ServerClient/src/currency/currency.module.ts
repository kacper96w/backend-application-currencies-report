import { Module } from '@nestjs/common';
import { CurrencyService } from './currency.service';
import { CurrencyController } from './currency.controller';
import { UsersModule } from 'src/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CurrencyEntity } from './currency.entity';

@Module({
  imports: [UsersModule, TypeOrmModule.forFeature([CurrencyEntity])],
  providers: [CurrencyService],
  controllers: [CurrencyController],
})
export class CurrencyModule {}
