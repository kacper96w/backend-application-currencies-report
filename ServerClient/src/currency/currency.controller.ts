import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { error } from 'console';
import {
  CreateCurrencyDto,
  CurrencyEntity,
  ErrorResponse,
  GetCurrenciesDto,
} from './currency.entity';
import { CurrencyService } from './currency.service';

@Controller('currency')
@ApiTags('Currencies')
@ApiResponse({
  status: 500,
  type: ErrorResponse,
  description: '[CURRENCY.CONTROLLER] Internal Server Error',
})
export class CurrencyController {
  constructor(private currencyService: CurrencyService) {}

  @Get('findByFilter')
  @ApiOkResponse({ type: CurrencyEntity })
  @UsePipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
      whitelist: false,
    }),
  )
  async findBy(@Query() query): Promise<CurrencyEntity[]> {
    try {
      const currencies = await this.currencyService.findBy(query);
      return currencies;
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error:
            'currency/findByFilter allowed query parameters: id, added_time, name, currency_value',
        },
        HttpStatus.BAD_REQUEST,
        {
          // cause: error
        },
      );
    }
  }

  @Get()
  @ApiOkResponse({ type: CurrencyEntity })
  @UsePipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  )
  async findAll(@Query() query: GetCurrenciesDto): Promise<CurrencyEntity[]> {
    console.log('[CURRENCY.CONTROLLER] Query: ', query);
    const currencies = await this.currencyService.getAllCurrencies();
    return currencies;
  }

  @Post()
  @ApiOkResponse({ type: CurrencyEntity })
  async create(
    @Body(new ValidationPipe()) data: CreateCurrencyDto,
  ): Promise<CurrencyEntity> {
    const allCurrencies = await this.currencyService.getAllCurrencies();
    const existingCurrency = allCurrencies.find(
      (currency) => currency.added_time === data.added_time,
    );

    if (existingCurrency) {
      throw new BadRequestException(
        `[CURRENCY.CONTROLLER] added_time of currency is already saved.`,
      );
    }

    const currency = await this.currencyService.create(data);

    return currency;
  }
}
