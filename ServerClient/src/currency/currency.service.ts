import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CurrencyEntity } from './currency.entity';

@Injectable()
export class CurrencyService {
  constructor(
    @InjectRepository(CurrencyEntity)
    private currencyRepository: Repository<CurrencyEntity>,
  ) {}

  async findOne(name: string): Promise<CurrencyEntity | null> {
    return this.currencyRepository.findOneBy({ name });
  }

  async getAllCurrencies(): Promise<CurrencyEntity[]> {
    const currencies = await this.currencyRepository.find();
    return currencies;
  }

  async findBy(query: Partial<CurrencyEntity>): Promise<CurrencyEntity[]> {
    return this.currencyRepository.find({ where: query });
  }

  async create(data: Partial<CurrencyEntity>): Promise<CurrencyEntity> {
    const currency = this.currencyRepository.create(data);
    await this.currencyRepository.save(currency);

    return currency;
  }
}
