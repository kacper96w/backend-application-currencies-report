import { Role, User } from './user.entity';
import { CurrencyEntity } from 'src/currency/currency.entity';

const entities = [User, Role, CurrencyEntity];

export { User };
export default entities;
