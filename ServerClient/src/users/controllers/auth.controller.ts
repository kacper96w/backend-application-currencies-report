import {
  Controller,
  Get,
  UseInterceptors,
  ClassSerializerInterceptor,
  Body,
  ValidationPipe,
  Post,
  BadRequestException,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PerformanceInterceptor } from '../interceptors/performance.interceptor';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { Auth } from '../decorators/auth.decorator';
import { User } from 'src/typeorm';
import { ApiAuth } from '../decorators/api-auth.decorator';
import {
  AuthLoginDto,
  AuthLoginResponse,
  AuthRegisterDto,
} from '../dto/auth.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @Get()
  @ApiAuth()
  @UseInterceptors(PerformanceInterceptor, ClassSerializerInterceptor)
  me(@Auth() user: User) {
    return user;
  }

  @Post('register')
  async register(@Body(new ValidationPipe()) data: AuthRegisterDto) {
    const [exists] = await this.usersService.findBy({ email: data.email });

    if (exists) {
      throw new BadRequestException(
        `[AUTH.CONTROLLER] Email ${data.email} already taken.`,
      );
    }

    const password = await this.authService.encodePassword(data.password);

    const user = await this.usersService.createUser({
      ...data,
      password,
    });

    return user;
  }

  @Post('login')
  async login(@Body(new ValidationPipe()) data: AuthLoginDto) {
    const user = await this.authService.validateUser(data.email, data.password);

    if (!user) {
      throw new BadRequestException(
        '[AUTH.CONTROLLER] Credentials are invalid.',
      );
    }

    const token = await this.authService.encodeUserToken(user);

    return { token, user };
  }
}
