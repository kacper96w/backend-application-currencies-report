import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';
import { User } from 'src/typeorm';

export class AuthRegisterDto {
  @ApiProperty({ example: 'Piotr' })
  name: string;

  @ApiProperty({ example: 'piotr@myflow.pl' })
  @IsEmail()
  email: string;

  @ApiProperty({ example: '!@#$' })
  @IsPassword()
  password: string;
}

export class AuthLoginDto {
  
  @ApiProperty({example: 'kacper.wozniak@winylownia.pl'})
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsPassword()
  password: string;
}

export class AuthLoginResponse {
  token: string;
  user: User;
}

export function IsPassword(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isPassword',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: {
        message:
          '[AUTH.DTO] Password has to be equall or grater then 4 characters.',
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          return ('' + value).length >= 4;
        },
      },
    });
  };
}
