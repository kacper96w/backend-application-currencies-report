import { ExceptionResponse, RoleNames } from 'src/typeorm/user.entity';
import { applyDecorators, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { Roles } from './roles.decorator';

export const ApiAuth = (...roles: RoleNames[]) =>
  applyDecorators(
    UseGuards(JwtAuthGuard),
    Roles(...roles),
    ApiBearerAuth(),
    ApiResponse({
      status: 401,
      type: ExceptionResponse,
      description: '[API-AUTH.DECORATOR] JWT token required.',
    }),
    ApiResponse({
      status: 403,
      type: ExceptionResponse,
      description: '[API-AUTH.DECORATOR] Some extra roles required.',
    }),
  );
