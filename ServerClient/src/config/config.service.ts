import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { resolve } from 'path';
import { validateOrReject } from 'class-validator';
import { stat } from 'fs/promises';

@Injectable()
export class ConfigService implements OnModuleInit {
  private logger = new Logger('ConfigService');
  readonly STORAGE_DIR = resolve('src', 'storage');
  readonly JWT_SECRET = process.env.JWT_SECRET;

  async onModuleInit() {
    // validation of config
    await validateOrReject(this).catch((errors) => {
      errors.forEach((e) =>
        this.logger.error(Object.values(e.constraints).join(', ')),
      );
      throw errors;
    });

    // validation of storage dir existence
    await stat(resolve(this.STORAGE_DIR, '.storage')).catch((err) => {
      this.logger.error(
        `STORAGE_DIR location should exists !!! tested: ${this.STORAGE_DIR}`,
      );
      throw err;
    });
  }
}
