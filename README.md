<h1 align="center">
  Currency App (NestJS API)
</h1>

<p align="center"> This repository is Backend API part of Currency App written in NestJS. This project includes API for Currency Entities Managment, Authentication, User Managment, Role Managment and others.
<br>
<p align="center">

<p align="center">
  <sub>Created and maintained by <a href="https://www.linkedin.com/in/kacper-wo%C5%BAniak-408aa5192/">Kacper Wozniak.</a></sub>
</p>

## Table of Contents

- [Getting Started](#getting-started)
- [Prerequisites](#Prerequisites)
- [Available Scripts](#available-scripts)
- [Setup](#setup)
- [Docker Setup](#docker-setup)

---


## Getting Started

This project stores and manage currency rates from a official Central Bank of Poland - NBP website once a day and save them in the database. There are a few endpoints in the currency controller to control the data. The authentication and roles are implemented as well.

Nested Virtualization in the Virtual Machine created in Google Cloud Platform is used to deploy the application - it is running in the Docker Container in the VM.

There is a second script created to get the currency rates URL from Central Bank of Poland - NBP, set proper body and store the data in the application database. To do that I used Jenkins automatization environment in VM, where I run the script once a day.



---

## Prerequisites

NodeJS
https://nodejs.org/en/

Typescript
https://www.typescriptlang.org/

PostgresQL
https://www.postgresql.org/

Docker
https://www.docker.com/

Jenkins
https://www.jenkins.io/

---

## Available Scripts

In the project directory, you can run:

### `npm run start:dev`

Runs the app in the development & watch mode.<br>
Open [http://localhost:3000/api](http://localhost:3000/api) to view swagger API docs in browser (only available in development mode).<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>


---

## Setup

First, you need to clone the project using

```bash
git clone https://gitlab.com/kacper96w/backend-application-currencies-report.git
```
To run the application locally in development environment use command:
```bash
npm run start:dev
```
---
## Docker Setup

**If you want to run project without docker you will not need to create .env file**

If you want to use **Docker** to deploy it on production or development stage
First create a .env file copying from .env.example and add environment for below parameters only since it will be used for building container using docker-compose

```env
SERVER_PORT=3000
DB_PASSWORD=790889097
DB_USERNAME=postgres
DB_DATABASE_NAME=work_db2
DB_PORT=5430
```

After creating env file make changes in configuration in accordance with you development environment. Follow setup guide in case you missed it.
 
Now to run containers do
```bash
docker-compose build .
docker-compose up -d
```
These commands will run 2 containers for PostgresQL and Main API.

---