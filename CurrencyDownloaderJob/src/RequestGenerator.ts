import flow from 'xml-flow';
import fs from 'fs';
import { ISingleCurrencyData } from './types/RequestGenerator.types';
import { DateTime } from 'luxon';
import axios from 'axios';

export class RequestGenerator {
  public async generatePostRequests(): Promise<void> {
    const readStream = fs.createReadStream('./currenciesValues.xml');

    const xmlStream = flow(readStream);

    xmlStream.on('tag:pozycja', async (currencyData) => {
      const body = this.setPostBodyRequest(currencyData);
      await axios
        .post('http://localhost:3000/currency', body, {
          timeout: 8000,
        })
        .catch((error) => {
          console.error(
            `${error.message}\n${error.response.data.message}`,
          );
        });
    });
  }

  private setPostBodyRequest(
    currencyData: Record<string, string>,
  ): ISingleCurrencyData {
    const name = currencyData?.kod_waluty;
    const currencyValue = currencyData?.kurs_sredni.replace(',', '.');
    const addedTime = DateTime.now().toFormat('yyyy-MM-dd');

    return {
      name: name,
      currency_value: currencyValue,
      added_time: addedTime,
    };
  }
}
