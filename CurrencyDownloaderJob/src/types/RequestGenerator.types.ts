export interface ISingleCurrencyData {
    name: string;
    currency_value: string;
    added_time: string;
}