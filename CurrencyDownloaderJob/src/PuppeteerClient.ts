import puppeteer from 'puppeteer';

export class ApiController {
  public async getCurrenciesRateUrl(): Promise<string> {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://www.nbp.pl/home.aspx?f=/kursy/kursya.html');
    // const a = (await page.$x("//*[text()='Powyższa tabela w formacie .xml']"))[0];

    const elementHandles = await page.$$('a');
    const propertyJsHandles = await Promise.all(
      elementHandles.map((handle) => handle.getProperty('href')),
    );

    const hrefs = await Promise.all(
      propertyJsHandles.map((handle) => handle.jsonValue()),
    );

    const currenciesRateUrl = hrefs.filter((href) => href.includes('xml'))[0];

    await browser.close();

    return currenciesRateUrl;
  }
}
