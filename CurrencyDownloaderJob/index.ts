import axios from 'axios';
import { ApiController } from './src/PuppeteerClient';
import fs from 'fs';
import { RequestGenerator } from './src/RequestGenerator';

(async () => {
  const api = new ApiController();
  const rg = new RequestGenerator();

  console.log(`[LOG] Getting currencies rate url from NBP page...`);
  const currenciesRateUrl = await api.getCurrenciesRateUrl();

  console.log(`[LOG] Saving XML file...`);
  const { data: currenciesValues }: { data: string } = await axios.get(
    currenciesRateUrl,
  );
  fs.writeFileSync('currenciesValues.xml', currenciesValues);

  console.log(`[LOG] Generating post requests to Currency App...`);
  await rg.generatePostRequests();
})().catch((error) => {
  if (error instanceof Error) {
    console.log(error.message);
  } else {
    console.log(error);
  }
  process.exit(1);
});
